
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installing
 * Permissions
 * Configuration
 * Troubleshooting
 * FAQ
 * How Can You Contribute?
 * Maintainers & Credits

INTRODUCTION
------------

IP Path Access is an access control module which allows an administrator 
to set a path and IP (or IP range) and prevent all users from accessing 
the path that are not accessing from the set IP address (or IP range).

In other words, the module is intended to permit access to the path and 
IP set, and deny for all others.

There are permissions available to specify which roles can add and 
delete IP settings, as well as user roles who can bypass the IP blocking.

Multiple IP and path pairs can be set, as long as they do not exactly match.

The module also has an option for Organic Groups (OG), 
so that users who are in the Group for a node are allowed access, 
if the OG option is set.

REQUIREMENTS
--------------

This module has no dependencies

INSTALLING
----------

* Install as you would normally install a contributed Drupal module.
See:
  https://www.drupal.org/docs/7/extending-drupal-7/
installing-contributed-modules-find-import-enable-configure-drupal-7
  for further information.


PERMISSIONS
------------
Visit /admin/people/permissions, 
or via the Admin menu: People > Permissions

Scroll down to the IP Path Access permission group.

There are two permissions:

 * Administer IP addresses by path
 * Bypass IP address access by path rules

Set permissions accordingly for your website use case.

CONFIGURATION
------------

Visit /admin/config/people/ip_path_access, 
or via the Admin menu: Configuration > People > IP Path Access

On the module admin page

1. Enable the 'Enable IP access by path' checkbox
When enabled, path/IP pairs below will be allowed, all other blocked.

2. Title: Enter a Title for your IP Path Access rule

3. IP address: Enter IP address in the format of, 
Single IP number: 1.2.3.4
IP range: 1.2.3.4 - 5.6.7.8

4. Path: Enter a valid path.

5. Custom redirect: Enter a valid path to redirect the user 
when denied access.

6. Redirect message: Enter a warning message to display when the user 
is redirected to the custom redirect path. Leave this text area blank to 
not display a warning message to the user.


TROUBLESHOOTING
---------------


FAQ
---

- There are no frequently asked questions at this time.


HOW CAN YOU CONTRIBUTE?
-----------------------

 * Report any bugs, feature requests, etc. in the issue tracker.
  https://www.drupal.org/project/issues/ip_path_access


MAINTAINERS & CREDITS
---------------------

 * Current Maintainer: https://www.drupal.org/u/darrell_ulm
 * Co-maintainer: https://www.drupal.org/u/mikebrooks
 * Co-maintainer: https://www.drupal.org/u/bhidenishad
